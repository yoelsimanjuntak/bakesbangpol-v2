# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: tt_bakesbangpol
# Generation Time: 2023-09-27 08:38:19 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _faqs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_faqs`;

CREATE TABLE `_faqs` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Question` text,
  `Answer` text,
  `Name` varchar(200) DEFAULT '',
  `Timestamp` datetime NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_faqs` WRITE;
/*!40000 ALTER TABLE `_faqs` DISABLE KEYS */;

INSERT INTO `_faqs` (`Uniq`, `Question`, `Answer`, `Name`, `Timestamp`)
VALUES
	(2,'Apakah pendaftaran izin dapat dilakukan secara online?','Ya, anda dapat melakukan pendaftaran secara online melalui Aplikasi SiCantik untuk lebih jelas anda dapat mengunduh panduan aplikasi sicantik melalui link ini.','','2023-05-01 12:00:00'),
	(3,'Bagaimana pemohon mengetahui status proses perizinan yang dimohonkan?','Anda dapat melakukan tracking permohonan izin melalui akun yang telah didaftarkan melalui Aplikasi SiCantik Cloud atau anda juga dapat melakukan tracking permohonan melalui Tracking Permohonan dengan menginput nomor pendaftaran anda.','','2023-05-01 12:00:00'),
	(4,'Bagaimana melakukan pendaftaran perizinan berusaha?','Anda dapat melakukan pendaftaran melalui OSS RBA dengan terlebih dahulu mengetahui kode KBLI kegiatan usaha anda.','','2023-05-01 12:00:00'),
	(5,'Bagaimana mengetahui kode KBLI kegiatan usaha?','Anda dapat mengakses Informasi Kode KBLI melalui link https://oss.go.id/informasi/kbli-berbasis-risiko.','','2023-05-01 12:00:00');

/*!40000 ALTER TABLE `_faqs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _homepage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_homepage`;

CREATE TABLE `_homepage` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ContentID` varchar(20) DEFAULT NULL,
  `ContentTitle` varchar(200) DEFAULT NULL,
  `ContentType` varchar(50) NOT NULL,
  `ContentDesc1` text,
  `ContentDesc2` text,
  `ContentDesc3` text,
  `ContentDesc4` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_homepage` WRITE;
/*!40000 ALTER TABLE `_homepage` DISABLE KEYS */;

INSERT INTO `_homepage` (`Uniq`, `ContentID`, `ContentTitle`, `ContentType`, `ContentDesc1`, `ContentDesc2`, `ContentDesc3`, `ContentDesc4`)
VALUES
	(1,'TxtWelcome1','Sambutan & Foto','TEXT','<p style=\"text-align:center\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','k_dinkes2.png',NULL,NULL),
	(2,'TxtWelcome2','Struktur Organisasi','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','STRUKTURORGANISASI.jpeg',NULL,NULL),
	(41,'NumProfile','NUMBER 1','NUMBER','Angka 1',NULL,'1','Satuan'),
	(42,'NumProfile','NUMBER 2','NUMBER','Angka 2',NULL,'2','Satuan'),
	(43,'NumProfile','NUMBER 3','NUMBER','Angka 3',NULL,'3','Satuan'),
	(44,'NumProfile','NUMBER 4','NUMBER','Angka 4',NULL,'4','Satuan'),
	(45,'TxtPopup1','Tugas Pokok dan Fungsi','PDF',NULL,'rekomsaranakesehatan.pdf',NULL,NULL),
	(46,'TxtPopup1','Maklumat Pelayanan','IMG',NULL,'rekomsaranakesehatan1.pdf',NULL,NULL),
	(47,'TxtPopup1','Standar Pelayanan','PDF',NULL,'rekomsaranakesehatan2.pdf',NULL,NULL),
	(49,'TxtPopup2','Kesehatan Masyarakat','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(50,'TxtPopup2','Pencegahan & Pengendalian Penyakit','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(51,'TxtPopup2','Pelayanan & Sumber Daya Kesehatan','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(52,'TxtPopup3','Instalasi Farmasi','DB','select * from munit where UnitTipe = \'FARMASI\' order by UnitNama','UnitNama','Uniq',NULL),
	(53,'TxtPopup3','Puskesmas','DB','select * from munit where UnitTipe = \'PUSKESMAS\' order by UnitNama','UnitNama','Uniq',NULL),
	(55,'Carousel','Carousel 1','IMG',NULL,NULL,NULL,NULL),
	(56,'Carousel','Carousel 2','IMG',NULL,NULL,NULL,NULL),
	(57,'Carousel','Carousel 3','IMG',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `_homepage` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_logs`;

CREATE TABLE `_logs` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Timestamp` datetime DEFAULT NULL,
  `URL` text,
  `ClientInfo` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_logs` WRITE;
/*!40000 ALTER TABLE `_logs` DISABLE KEYS */;

INSERT INTO `_logs` (`Uniq`, `Timestamp`, `URL`, `ClientInfo`)
VALUES
	(1,'2023-09-27 10:04:50','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(2,'2023-09-27 10:05:45','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(3,'2023-09-27 10:06:13','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(4,'2023-09-27 10:07:40','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(5,'2023-09-27 10:07:43','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(6,'2023-09-27 10:09:00','http://localhost/tt-bakesbangpol/site/home/page/standar-pelayanan.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(7,'2023-09-27 10:09:07','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(8,'2023-09-27 10:11:19','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(9,'2023-09-27 10:11:26','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(10,'2023-09-27 10:11:59','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(11,'2023-09-27 10:12:21','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(12,'2023-09-27 10:13:42','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(13,'2023-09-27 10:13:54','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(14,'2023-09-27 10:16:32','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(15,'2023-09-27 10:17:11','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(16,'2023-09-27 10:17:47','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(17,'2023-09-27 10:18:32','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(18,'2023-09-27 10:19:48','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(19,'2023-09-27 10:24:35','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(20,'2023-09-27 10:30:29','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(21,'2023-09-27 10:31:26','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(22,'2023-09-27 10:31:38','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(23,'2023-09-27 10:31:44','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(24,'2023-09-27 10:31:55','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(25,'2023-09-27 10:32:04','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(26,'2023-09-27 11:46:06','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(27,'2023-09-27 11:46:21','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(28,'2023-09-27 11:46:31','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(29,'2023-09-27 11:46:36','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(30,'2023-09-27 11:51:49','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(31,'2023-09-27 11:56:46','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(32,'2023-09-27 11:57:23','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(33,'2023-09-27 12:27:38','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(34,'2023-09-27 12:28:07','http://localhost/tt-bakesbangpol/site/home/page/struktur-organisasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(35,'2023-09-27 12:28:08','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(36,'2023-09-27 12:28:10','http://localhost/tt-bakesbangpol/site/home/page/tupoksi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(37,'2023-09-27 12:28:11','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(38,'2023-09-27 12:28:13','http://localhost/tt-bakesbangpol/site/home/page/pegawai.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(39,'2023-09-27 12:28:14','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(40,'2023-09-27 12:34:08','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(41,'2023-09-27 12:35:01','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(42,'2023-09-27 12:36:21','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(43,'2023-09-27 12:36:57','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(44,'2023-09-27 12:37:21','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(45,'2023-09-27 12:38:15','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(46,'2023-09-27 12:38:40','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(47,'2023-09-27 12:38:50','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(48,'2023-09-27 12:39:11','http://localhost/tt-bakesbangpol/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(49,'2023-09-27 12:39:45','http://localhost/tt-bakesbangpol/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(50,'2023-09-27 12:40:07','http://localhost/tt-bakesbangpol/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(51,'2023-09-27 12:40:17','http://localhost/tt-bakesbangpol/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(52,'2023-09-27 12:40:33','http://localhost/tt-bakesbangpol/site/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(53,'2023-09-27 12:40:38','http://localhost/tt-bakesbangpol/site/post/index/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(54,'2023-09-27 12:40:41','http://localhost/tt-bakesbangpol/site/post/index/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(55,'2023-09-27 12:40:44','http://localhost/tt-bakesbangpol/site/post/add/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(56,'2023-09-27 12:40:47','http://localhost/tt-bakesbangpol/site/post/index/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(57,'2023-09-27 12:41:29','http://localhost/tt-bakesbangpol/site/post/index/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(58,'2023-09-27 12:41:33','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(59,'2023-09-27 12:41:35','http://localhost/tt-bakesbangpol/site/post/index/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(60,'2023-09-27 12:41:39','http://localhost/tt-bakesbangpol/site/post/index/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(61,'2023-09-27 12:41:40','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(62,'2023-09-27 12:42:04','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(63,'2023-09-27 12:42:05','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(64,'2023-09-27 12:42:07','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(65,'2023-09-27 12:42:15','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(66,'2023-09-27 12:42:45','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(67,'2023-09-27 12:43:02','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(68,'2023-09-27 12:43:15','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(69,'2023-09-27 12:43:29','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(70,'2023-09-27 12:45:03','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(71,'2023-09-27 12:45:05','http://localhost/tt-bakesbangpol/site/post/index/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(72,'2023-09-27 12:45:06','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(73,'2023-09-27 12:45:07','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(74,'2023-09-27 12:45:11','http://localhost/tt-bakesbangpol/site/post/index/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(75,'2023-09-27 12:45:13','http://localhost/tt-bakesbangpol/site/post/index/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(76,'2023-09-27 12:45:18','http://localhost/tt-bakesbangpol/site/post/index/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(77,'2023-09-27 12:45:20','http://localhost/tt-bakesbangpol/site/post/index/5.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(78,'2023-09-27 12:45:38','http://localhost/tt-bakesbangpol/site/home/page/maklumat-pelayanan.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(79,'2023-09-27 12:45:50','http://localhost/tt-bakesbangpol/site/home/page/potensi-investasi-kota-tebing-tinggi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(80,'2023-09-27 12:46:01','http://localhost/tt-bakesbangpol/site/post/index/5.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(81,'2023-09-27 12:46:03','http://localhost/tt-bakesbangpol/site/home/page/struktur-organisasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(82,'2023-09-27 12:46:11','http://localhost/tt-bakesbangpol/site/post/edit/21.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(83,'2023-09-27 12:46:35','http://localhost/tt-bakesbangpol/site/post/index/5.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(84,'2023-09-27 12:47:01','http://localhost/tt-bakesbangpol/site/post/index/5.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(85,'2023-09-27 12:47:07','http://localhost/tt-bakesbangpol/site/post/index/5.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(86,'2023-09-27 12:47:09','http://localhost/tt-bakesbangpol/site/post/index/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(87,'2023-09-27 12:52:50','http://localhost/tt-bakesbangpol/site/home/page/skm.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(88,'2023-09-27 12:52:52','http://localhost/tt-bakesbangpol/site/home/page/maklumat-pelayanan.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(89,'2023-09-27 12:52:54','http://localhost/tt-bakesbangpol/site/home/page/sakip.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(90,'2023-09-27 12:53:15','http://localhost/tt-bakesbangpol/site/home/page/maklumat-pelayanan.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(91,'2023-09-27 12:53:17','http://localhost/tt-bakesbangpol/site/home/page/skm.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(92,'2023-09-27 12:53:20','http://localhost/tt-bakesbangpol/site/home/page/maklumat-pelayanan.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(93,'2023-09-27 12:53:27','http://localhost/tt-bakesbangpol/site/home/page/skm.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(94,'2023-09-27 12:53:32','http://localhost/tt-bakesbangpol/site/home/page/maklumat-pelayanan.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(95,'2023-09-27 12:53:38','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(96,'2023-09-27 12:53:49','http://localhost/tt-bakesbangpol/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(97,'2023-09-27 12:53:51','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(98,'2023-09-27 12:54:02','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(99,'2023-09-27 12:54:05','http://localhost/tt-bakesbangpol/site/home/post/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(100,'2023-09-27 12:54:11','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(101,'2023-09-27 12:55:33','http://localhost/tt-bakesbangpol/site/post/index/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(102,'2023-09-27 12:58:01','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(103,'2023-09-27 12:58:02','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(104,'2023-09-27 13:01:03','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(105,'2023-09-27 13:01:03','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(106,'2023-09-27 13:01:11','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(107,'2023-09-27 13:01:42','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(108,'2023-09-27 13:02:13','http://localhost/tt-bakesbangpol/site/post/edit/24.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(109,'2023-09-27 13:03:16','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(110,'2023-09-27 13:03:17','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(111,'2023-09-27 13:03:19','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(112,'2023-09-27 13:03:21','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(113,'2023-09-27 13:04:52','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(114,'2023-09-27 13:06:14','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(115,'2023-09-27 13:06:31','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(116,'2023-09-27 13:06:44','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(117,'2023-09-27 13:06:57','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(118,'2023-09-27 13:07:16','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(119,'2023-09-27 13:07:21','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(120,'2023-09-27 13:08:23','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(121,'2023-09-27 13:08:32','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(122,'2023-09-27 13:08:42','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(123,'2023-09-27 13:10:36','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(124,'2023-09-27 13:11:05','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(125,'2023-09-27 13:11:32','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(126,'2023-09-27 13:11:37','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(127,'2023-09-27 13:11:55','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(128,'2023-09-27 13:12:02','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(129,'2023-09-27 13:12:42','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(130,'2023-09-27 13:13:40','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(131,'2023-09-27 13:13:51','http://localhost/tt-bakesbangpol/site/post/edit/24.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(132,'2023-09-27 13:14:01','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(133,'2023-09-27 13:14:11','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(134,'2023-09-27 13:14:37','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(135,'2023-09-27 13:15:04','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(136,'2023-09-27 13:16:19','http://localhost/tt-bakesbangpol/site/post/edit/24.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(137,'2023-09-27 13:16:19','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(138,'2023-09-27 13:16:23','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(139,'2023-09-27 13:17:04','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(140,'2023-09-27 13:18:16','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(141,'2023-09-27 13:18:17','http://localhost/tt-bakesbangpol/site/home/page/images/logos/google.png.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(142,'2023-09-27 13:18:52','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(143,'2023-09-27 13:18:52','http://localhost/tt-bakesbangpol/site/home/page/images/logos/google.png.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(144,'2023-09-27 13:20:43','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(145,'2023-09-27 13:20:44','http://localhost/tt-bakesbangpol/site/home/page/images/logos/google.png.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(146,'2023-09-27 13:21:32','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(147,'2023-09-27 13:21:32','http://localhost/tt-bakesbangpol/site/home/page/images/logos/google.png.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(148,'2023-09-27 13:21:49','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(149,'2023-09-27 13:22:18','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(150,'2023-09-27 13:22:22','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(151,'2023-09-27 13:22:29','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(152,'2023-09-27 13:22:50','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(153,'2023-09-27 13:23:04','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(154,'2023-09-27 13:23:31','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(155,'2023-09-27 13:23:32','http://localhost/tt-bakesbangpol/site/home/page/images/logos/google.png.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(156,'2023-09-27 13:23:58','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(157,'2023-09-27 13:23:58','http://localhost/tt-bakesbangpol/site/home/page/images/logos/google.png.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(158,'2023-09-27 13:24:52','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(159,'2023-09-27 13:24:52','http://localhost/tt-bakesbangpol/site/home/page/images/logos/google.png.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(160,'2023-09-27 13:24:56','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(161,'2023-09-27 13:24:56','http://localhost/tt-bakesbangpol/site/home/page/images/logos/google.png.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(162,'2023-09-27 13:25:24','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(163,'2023-09-27 13:25:24','http://localhost/tt-bakesbangpol/site/home/page/images/logos/google.png.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(164,'2023-09-27 13:27:05','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(165,'2023-09-27 13:27:05','http://localhost/tt-bakesbangpol/site/home/page/images/logos/google.png.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(166,'2023-09-27 13:27:11','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(167,'2023-09-27 13:27:17','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(168,'2023-09-27 13:27:46','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(169,'2023-09-27 13:27:55','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(170,'2023-09-27 13:28:03','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(171,'2023-09-27 13:28:59','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(172,'2023-09-27 13:29:07','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(173,'2023-09-27 13:30:06','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(174,'2023-09-27 13:30:07','http://localhost/tt-bakesbangpol/site/home/page/images/logos/google.png.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(175,'2023-09-27 13:30:11','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(176,'2023-09-27 13:30:21','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(177,'2023-09-27 13:30:25','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(178,'2023-09-27 13:30:44','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(179,'2023-09-27 13:38:02','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(180,'2023-09-27 13:40:29','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(181,'2023-09-27 13:42:25','http://localhost/tt-bakesbangpol/site/home/page/sakip.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(182,'2023-09-27 13:43:14','http://localhost/tt-bakesbangpol/site/home/page/sakip.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(183,'2023-09-27 13:57:55','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(184,'2023-09-27 13:58:44','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(185,'2023-09-27 13:59:11','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(186,'2023-09-27 13:59:19','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(187,'2023-09-27 13:59:24','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(188,'2023-09-27 13:59:50','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(189,'2023-09-27 14:00:06','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(190,'2023-09-27 14:00:21','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(191,'2023-09-27 14:00:30','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(192,'2023-09-27 14:02:08','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(193,'2023-09-27 14:03:28','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(194,'2023-09-27 14:04:38','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(195,'2023-09-27 14:05:44','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(196,'2023-09-27 14:05:57','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(197,'2023-09-27 14:06:11','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(198,'2023-09-27 14:06:15','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(199,'2023-09-27 14:06:41','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(200,'2023-09-27 14:06:56','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(201,'2023-09-27 14:07:33','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(202,'2023-09-27 14:07:55','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(203,'2023-09-27 14:08:17','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(204,'2023-09-27 14:08:33','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(205,'2023-09-27 14:09:37','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(206,'2023-09-27 14:09:55','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(207,'2023-09-27 14:09:59','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(208,'2023-09-27 14:10:18','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(209,'2023-09-27 14:10:54','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(210,'2023-09-27 14:12:14','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(211,'2023-09-27 14:13:01','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(212,'2023-09-27 14:13:14','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(213,'2023-09-27 14:13:21','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(214,'2023-09-27 14:14:30','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(215,'2023-09-27 14:14:37','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(216,'2023-09-27 14:15:09','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(217,'2023-09-27 14:15:20','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(218,'2023-09-27 14:15:35','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(219,'2023-09-27 14:16:14','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(220,'2023-09-27 14:17:48','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(221,'2023-09-27 14:18:13','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(222,'2023-09-27 14:18:16','http://localhost/tt-bakesbangpol/site/post/edit/24.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(223,'2023-09-27 14:18:32','http://localhost/tt-bakesbangpol/site/post/edit/24.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(224,'2023-09-27 14:18:33','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(225,'2023-09-27 14:18:36','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(226,'2023-09-27 14:19:03','http://localhost/tt-bakesbangpol/site/post/edit/24.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(227,'2023-09-27 14:20:01','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(228,'2023-09-27 14:20:18','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(229,'2023-09-27 14:24:12','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(230,'2023-09-27 14:24:12','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(231,'2023-09-27 14:24:14','http://localhost/tt-bakesbangpol/site/post/edit/24.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(232,'2023-09-27 14:24:18','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(233,'2023-09-27 14:24:19','http://localhost/tt-bakesbangpol/site/post/edit/25.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(234,'2023-09-27 14:31:51','http://localhost/tt-bakesbangpol/site/post/edit/25.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(235,'2023-09-27 14:31:51','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(236,'2023-09-27 14:31:55','http://localhost/tt-bakesbangpol/site/home/page/gerakan-pembagian-bendera-merah-putih-tahun-2023.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(237,'2023-09-27 14:32:15','http://localhost/tt-bakesbangpol/site/home/page/gerakan-pembagian-bendera-merah-putih-tahun-2023.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(238,'2023-09-27 14:32:53','http://localhost/tt-bakesbangpol/site/home/page/gerakan-pembagian-bendera-merah-putih-tahun-2023.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(239,'2023-09-27 14:33:29','http://localhost/tt-bakesbangpol/site/post/edit/25.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(240,'2023-09-27 14:33:35','http://localhost/tt-bakesbangpol/site/post/edit/25.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(241,'2023-09-27 14:33:35','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(242,'2023-09-27 14:33:38','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(243,'2023-09-27 14:33:39','http://localhost/tt-bakesbangpol/site/home/page/gerakan-pembagian-bendera-merah-putih-tahun-2023.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(244,'2023-09-27 14:34:03','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(245,'2023-09-27 14:34:08','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(246,'2023-09-27 14:34:29','http://localhost/tt-bakesbangpol/site/home/page/badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(247,'2023-09-27 14:34:34','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(248,'2023-09-27 14:35:05','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(249,'2023-09-27 14:37:06','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(250,'2023-09-27 14:37:06','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(251,'2023-09-27 14:37:09','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(252,'2023-09-27 14:37:12','http://localhost/tt-bakesbangpol/site/home/page/pengukuhan-paskibraka-kota-tebing-tinggi-tahun-2023.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(253,'2023-09-27 14:37:24','http://localhost/tt-bakesbangpol/site/home/page/pengukuhan-paskibraka-kota-tebing-tinggi-tahun-2023.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(254,'2023-09-27 14:37:59','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(255,'2023-09-27 14:38:01','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(256,'2023-09-27 14:40:02','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(257,'2023-09-27 14:40:02','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(258,'2023-09-27 14:40:04','http://localhost/tt-bakesbangpol/site/home/page/pengukuhan-paskibraka-kota-tebing-tinggi-tahun-2023.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(259,'2023-09-27 14:40:24','http://localhost/tt-bakesbangpol/site/home/page/apel-kehormatan-dan-renungan-suci-kota-tebing-tinggi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(260,'2023-09-27 14:42:18','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(261,'2023-09-27 14:42:30','http://localhost/tt-bakesbangpol/site/home/page/apel-kehormatan-dan-renungan-suci-kota-tebing-tinggi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(262,'2023-09-27 14:43:12','http://localhost/tt-bakesbangpol/site/home/page/apel-kehormatan-dan-renungan-suci-kota-tebing-tinggi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(263,'2023-09-27 14:45:03','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(264,'2023-09-27 14:46:40','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(265,'2023-09-27 14:46:40','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(266,'2023-09-27 14:46:42','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(267,'2023-09-27 14:47:05','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(268,'2023-09-27 14:48:55','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(269,'2023-09-27 14:50:04','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(270,'2023-09-27 14:50:04','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(271,'2023-09-27 14:50:05','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(272,'2023-09-27 14:50:28','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(273,'2023-09-27 14:52:27','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(274,'2023-09-27 14:52:30','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(275,'2023-09-27 14:55:40','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(276,'2023-09-27 14:55:42','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(277,'2023-09-27 14:57:04','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(278,'2023-09-27 14:57:18','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(279,'2023-09-27 14:57:24','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(280,'2023-09-27 15:02:06','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(281,'2023-09-27 15:02:08','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(282,'2023-09-27 15:03:00','http://localhost/tt-bakesbangpol/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(283,'2023-09-27 15:03:00','http://localhost/tt-bakesbangpol/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(284,'2023-09-27 15:03:03','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(285,'2023-09-27 15:03:06','http://localhost/tt-bakesbangpol/site/home/page/studi-wisata-paskibraka-kota-tebing-tinggi-tahun-2023.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(286,'2023-09-27 15:03:51','http://localhost/tt-bakesbangpol/site/home/page/studi-wisata-paskibraka-kota-tebing-tinggi-tahun-2023.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(287,'2023-09-27 15:05:58','http://localhost/tt-bakesbangpol/site/home/page/studi-wisata-paskibraka-kota-tebing-tinggi-tahun-2023.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(288,'2023-09-27 15:06:21','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(289,'2023-09-27 15:06:24','http://localhost/tt-bakesbangpol/site/home/page/struktur-organisasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(290,'2023-09-27 15:07:06','http://localhost/tt-bakesbangpol/site/post/index/5.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(291,'2023-09-27 15:07:09','http://localhost/tt-bakesbangpol/site/post/edit/21.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(292,'2023-09-27 15:08:18','http://localhost/tt-bakesbangpol/site/post/index/5.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(293,'2023-09-27 15:08:20','http://localhost/tt-bakesbangpol/site/post/edit/21.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(294,'2023-09-27 15:08:22','http://localhost/tt-bakesbangpol/site/post/index/5.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(295,'2023-09-27 15:08:23','http://localhost/tt-bakesbangpol/site/post/edit/23.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(296,'2023-09-27 15:09:06','http://localhost/tt-bakesbangpol/site/post/index/5.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(297,'2023-09-27 15:12:09','http://localhost/tt-bakesbangpol/site/home/page/skm.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(298,'2023-09-27 15:12:12','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(299,'2023-09-27 15:12:36','http://localhost/tt-bakesbangpol/site/home/contact.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(300,'2023-09-27 15:13:14','http://localhost/tt-bakesbangpol/site/home/contact.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(301,'2023-09-27 15:13:17','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(302,'2023-09-27 15:13:46','http://localhost/tt-bakesbangpol/site/home/page/pegawai.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(303,'2023-09-27 15:13:48','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(304,'2023-09-27 15:23:36','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(305,'2023-09-27 15:23:56','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(306,'2023-09-27 15:24:22','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(307,'2023-09-27 15:25:01','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(308,'2023-09-27 15:29:11','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(309,'2023-09-27 15:29:45','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(310,'2023-09-27 15:30:43','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(311,'2023-09-27 15:31:00','http://localhost/tt-bakesbangpol/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36');

/*!40000 ALTER TABLE `_logs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postcategories`;

CREATE TABLE `_postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  `IsShowEditor` tinyint(1) NOT NULL DEFAULT '1',
  `IsAllowExternalURL` tinyint(1) NOT NULL DEFAULT '0',
  `IsDocumentOnly` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_postcategories` WRITE;
/*!40000 ALTER TABLE `_postcategories` DISABLE KEYS */;

INSERT INTO `_postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`, `IsShowEditor`, `IsAllowExternalURL`, `IsDocumentOnly`)
VALUES
	(2,'Berita','Berita & artikel terkini seputar Badan Kesbangpol',1,0,0),
	(4,'SKM','Survei Kepuasan Masyarakat',1,0,0),
	(3,'Galeri','Dokumentasi Kegiatan',1,0,0),
	(5,'Lainnya',NULL,1,0,0);

/*!40000 ALTER TABLE `_postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postimages`;

CREATE TABLE `_postimages` (
  `PostImageID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(20) NOT NULL,
  `ImgPath` text NOT NULL,
  `ImgDesc` varchar(250) NOT NULL,
  `ImgShortcode` varchar(50) NOT NULL,
  `IsHeader` tinyint(1) NOT NULL DEFAULT '1',
  `IsThumbnail` tinyint(1) NOT NULL DEFAULT '1',
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_postimages` WRITE;
/*!40000 ALTER TABLE `_postimages` DISABLE KEYS */;

INSERT INTO `_postimages` (`PostImageID`, `PostID`, `ImgPath`, `ImgDesc`, `ImgShortcode`, `IsHeader`, `IsThumbnail`, `Description`)
VALUES
	(10,10,'warga-mengantre-untuk-menukarkan-uang-rupiah-kertas-baru-tahun-emisi-te-2022-di-pasar-tebet-barat-jakarta-selatan-rabu-2482022-3_169.jpeg','Warga mengantre untuk menukarkan uang Rupiah kertas baru Tahun Emisi (TE) 2022 di Pasar Tebet Barat','',1,1,NULL),
	(11,11,'ombudsman_standarpelayananpublik_2.jpeg','PEMERINTAH KOTA TEBING TINGGI RAIH PERINGKAT 4 DENGAN KATEGORI KUALITAS TINGGI DENGAN JUMLAH NILAI 88,60','',1,1,NULL),
	(12,12,'64095576d728c.jpeg','Dok: Humas Kementan','',1,1,NULL),
	(16,24,'Snapinsta_app_366167561_1656193534803835_891598711005212340_n_1080.jpg','','',1,1,NULL),
	(19,24,'Snapinsta_app_366412340_6503702639705481_971375469705297049_n_1080.jpg','','',1,0,NULL),
	(20,25,'Snapinsta_app_366430916_683483466544541_4114270086354813784_n_1080.jpg','','',1,1,NULL),
	(21,25,'Snapinsta_app_366500353_155406344251716_3529762326351146102_n_1080.jpg','','',1,1,NULL),
	(22,25,'Snapinsta_app_366435932_155894654198971_2903344530725571446_n_1080.jpg','','@GAMBAR1@',0,0,NULL),
	(23,25,'Snapinsta_app_366555536_6549951611786997_4757031106657831145_n_1080.jpg','','@GAMBAR2@',0,0,NULL),
	(24,26,'Snapinsta_app_367444426_261467700079238_2819448126044209829_n_1080.jpg','','',1,1,NULL),
	(25,26,'Snapinsta_app_367938533_1145356033090702_9111138444585893290_n_1080.jpg','','',1,1,NULL),
	(26,27,'Snapinsta_app_367640753_1294361704520459_3481272315750332618_n_1080.jpg','','',1,1,NULL),
	(27,27,'Snapinsta_app_367511984_647326777353090_8439102173352993627_n_1080.jpg','','',1,1,NULL),
	(28,28,'Snapinsta_app_367961028_673371104825412_8888794767377596200_n_1080.jpg','','',1,1,NULL),
	(29,28,'Snapinsta_app_368145633_9782242485181589_7645688467974131163_n_1080.jpg','','',1,1,NULL),
	(30,29,'02.jpg','','',1,1,NULL),
	(31,29,'07.jpg','','',1,1,NULL),
	(32,30,'DSCF4478-min.jpg','','',1,1,NULL),
	(33,30,'DSCF4538.jpg','','',1,1,NULL);

/*!40000 ALTER TABLE `_postimages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_posts`;

CREATE TABLE `_posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(11) NOT NULL,
  `PostUnitID` int(11) DEFAULT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext,
  `PostExpiredDate` date DEFAULT NULL,
  `PostMetaTags` text,
  `IsRunningText` tinyint(1) NOT NULL DEFAULT '0',
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_posts` WRITE;
/*!40000 ALTER TABLE `_posts` DISABLE KEYS */;

INSERT INTO `_posts` (`PostID`, `PostCategoryID`, `PostUnitID`, `PostDate`, `PostTitle`, `PostSlug`, `PostContent`, `PostExpiredDate`, `PostMetaTags`, `IsRunningText`, `TotalView`, `LastViewDate`, `IsSuspend`, `FileName`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(10,1,NULL,'2023-05-01','Inflasi Ganas! Duit Sejuta di 2010 Setara Rp 1,7 Juta di 2023','inflasi-ganas-duit-sejuta-di-2010-setara-rp-1-7-juta-di-2023','<p>Tanpa selalu disadari, inflasi membuat nilai uang Rp 1 juta pada 2010 setara dengan Rp 1,73 juta pada 2023. Inflasi yang terus naik, apabila tidak dibarengi oleh peningkatan pendapatan, membuat nilai daya beli kita merosot.</p>\r\n\r\n<p>Hitung-hitungan tersebut berangkat dari data bahwa secara kumulatif tingkat inflasi selama 2010-2023 mencapai 73% atau setara dengan rerata 5,6% per tahun.</p>\r\n\r\n<p>Secara sederhana, inflasi dapat diartikan sebagai kenaikan harga barang dan jasa secara umum dan terus menerus dalam jangka waktu tertentu. Ketika harga barang naik, nilai daya beli menjadi merosot lantaran kita membutuhkan duit lebih banyak untuk membeli suatu barang.</p>\r\n\r\n<p>Artinya, inflasi berhubungan erat dengan penurunan nilai daya beli masyarakat. Semakin tinggi inflasi, nilai daya beli orang semakin turun.</p>\r\n\r\n<p>Daya beli adalah kemampuan masyarakat untuk membeli barang dan jasa dengan pendapatan yang dimiliki. Inflasi juga berkorelsi dengan nilai tukar rupiah terhadap dolar AS (Amerika Serikat), yang merupakan mata uang acuan dunia.</p>\r\n\r\n<p>Secara teori, inflasi yang tinggi cenderung menurunkan nilai tukar rupiah karena menurunkan daya beli relatif rupiah terhadap mata uang asing. Sebaliknya, nilai tukar rupiah yang melemah akan meningkatkan inflasi karena meningkatkan biaya impor barang dan jasa yang berdampak pada kenaikan harga.</p>\r\n\r\n<p>Penurunan nilai tukar rupiah terhadap dolar AS dapat berdampak negatif terhadap daya beli masyarakat. Sejak 2010, nilai tukar rupiah sudah merosot 57% secara akumulatif terhadap dolar.</p>\r\n\r\n<p>Kendati memang, selama 2023 (year to date/YtD), rupiah sedang menguat hingga 5,77% dibandingkan greenback AS. Penyebab utama inflasi sendiri ada dua, yakni kenaikan permintaan (sisi demand) dan kenaikan harga bahan baku (sisi supply).</p>\r\n\r\n<h3><strong>Pentingnya Investasi yang Tepat</strong></h3>\r\n\r\n<p>Inflasi memang tidak selalu buruk karena bisa menjadi indikasi pertumbuhan ekonomi suatu negara. Namun, inflasi yang terus meninggi apabila tidak dibarengi kemampuan daya beli yang ikut meningkat akan membuat masyarakat berpenghasilan rendah semakin tertekan.</p>\r\n\r\n<p>Untuk itu, selain terus berusaha menaikkan penghasilan (baik dari gaji maupun usaha), salah satu solusi untuk melindungi masyarakat dari dampak inflasi dan penurunan nilai tukar rupiah adalah melakukan investasi yang tepat.</p>\r\n\r\n<p>Investasi yang dimasuk bisa di aset riil--seperti properti, emas--maupun paper asset macam saham. Investasi dapat membantu masyarakat untuk meningkatkan pendapatan, mengamankan aset, dan mengantisipasi krisis ekonomi.</p>\r\n\r\n<p>Namun, perlu diingat, memilih investasi yang tepat berarti menanamkan modal di aset yang menawarkan imbal hasil di atas tingkat inflasi. Sebagai ilustrasi, apabila kita menyimpan dana, sebagai contoh, di aset senilai Rp 100 juta yang hanya memiliki bunga atau imbal hasil 4% tetapi laju inflasi mencapai 5%, itu artinya imbal hasil riil kita malah minus 1%.</p>\r\n\r\n<p>Singkatnya, nilai investasi kita malah turun Rp1 juta.<br />\r\nKarena itu, bagi para kaum pekerja yang tingkat kenaikan upahnya tergerus laju inflasi, berinvestasi di aset yang tepat bisa melindungi atau bahkan menambah penghasilan.<br />\r\nDengan asumsi rerata tingkat inflasi tahunan Indonesia 5%, itu artinya tingkat kenaikan gaji atau imbal hasil investasi setidaknya harus melampaui angka tersebut.</p>\r\n\r\n<p>Pilihan aset investasi beragam, seperti disebut di atas, mulai dari emas, saham, reksadana, atau properti.</p>\r\n\r\n<p><span style=\"font-size:10px\"><em>Sumber :&nbsp;https://www.cnbcindonesia.com/market/20230430110816-17-433318/inflasi-ganas-duit-sejuta-di-2010-setara-rp-17-juta-di-2023</em></span></p>\r\n',NULL,'inflasi,investasi,2023',0,81,NULL,0,NULL,'admin','2023-05-01 21:30:58','admin','2023-05-01 21:30:58'),
	(11,1,NULL,'2023-05-01','Pemko Tebing Tinggi Raih Zona Hijau Kepatuhan Standar Pelayanan Publik Tahun 2022','pemko-tebing-tinggi-raih-zona-hijau-kepatuhan-standar-pelayanan-publik-tahun-2022','<p>Pj. Wali Kota Tebing Tinggi, Muhammad Dimiyathi, S.Sos, M.TP menerima Penghargaan Predikat Kepatuhan Standar Pelayanan Publik Tahun 2022 dari Ombudsman RI Perwakilan Sumatera Utara, Kamis (26/1) di Kantor Ombudsman RI Perwakilan Sumatera Utara, Jalan Sei Besitang nomor 3 Medan.</p>\r\n\r\n<p>Penyerahan hasil penilaian penyelenggaraan pelayanan publik ini sesuai dengan surat Ombudsman RI Perwakilan Sumatera Utara Nomor : B/0006/PC.01.04-02/I/2023 20 Januari 2023 serta menindaklanjuti hasil Penilaian Penyelenggaraan Pelayanan Publik di 34 Pemerintah Daerah se Sumatera Utara Tahun 2022 sebagaimana telah diumumkan pada tanggal 22 Desember 2022 di Jakarta.&nbsp;</p>\r\n\r\n<p>Dari 33 pemerintah kabupaten/kota yang ada di Sumatera Utara, Pemerintah Kota Tebing Tinggi meraih peringkat 4 dengan kategori kualitas tinggi dengan jumlah nilai 88,60.</p>\r\n\r\n<p>Kepala Ombudsman RI Perwakilan Sumatera Utara Abyadi Siregar dalam sambutannya mengatakan dimana 10 besar pemerintah provinsi yang memperoleh nilai tertinggi se-Indonesia hadir secara langsung di Jakarta untuk menerima sertifikat penghargaan dari Ombudsman RI.</p>\r\n\r\n<p>Lebih lanjut dikatakan Kepala Ombudsman RI Perwakilan Sumatera Utara, dari 10 besar tersebut Provinsi Sumatera Utara mendapatkan peringkat 5 dari 34 Provinsi yang ada di Indonesia.&nbsp;</p>\r\n\r\n<p>Dijelaskan Kepala Ombudsman RI Perwakilan Sumatera Utara, jenis penilaian fokus pada pemeriksaan standar pelayanan publik yang merupakan ukuran baku yang wajib disediakan oleh penyelenggara pelayanan sebagai bentuk pemenuhan asas-asas transparansi dan akuntabilitas.&nbsp;</p>\r\n\r\n<p>Sebagaimana dinyatakan secara tegas dalam Pasal 54 Undang-Undang Nomor 25 Tahun 2009 tentang Pelayanan Publik, terdapat sanksi mulai dari sanksi pembebasan dari jabatan sampai dengan sanksi pembebasan dengan hormat tidak atas permintaan sendiri bagi pelaksana dan penyelenggara pelayanan publik yang tidak memenuhi kewajibannya menyediakan standar pelayanan publik yang layak ungkapnya.&nbsp;</p>\r\n\r\n<p>Dari penilaian tersebut, terbagi menjadi tiga zona yakni Hijau, Kuning dan Merah. Ada pun data kepala daerah kabupaten/kota yang mendapat predikat zona hijau diantaranya Bupati Deli Serdang, Bupati Humbang Hasundutan, Bupati Serdang Bedagai, Walikota Tebing Tinggi, Bupati Langkat, Bupati Tapanuli Selatan, Bupati Batu Bara, Bupati Nias, Bupati Pakpak Bharat, Bupati Simalungun, Bupati Dairi, Bupati Padang Lawas Utara, Walikota Medan, Bupati Tapanuli Utara, Bupati Labuhan Batu Utara.</p>\r\n\r\n<p>Sementara untuk predikat zona kuning yaitu Bupati Samosir, Bupati Nias Selatan, Bupati Toba, Bupati Asahan, Walikota Padangsidimpuan, Bupati Padang Lawas, Bupati Karo, Walikota Gunungsitoli, Bupati Tapanuli Tengah, Bupati Mandailing Natal, Bupati Labuhan Batu, Walikota Pematangsiantar, Bupati Nias Barat.<br />\r\nPredikat zona merah, Bupati Labuhan Batu Selatan, Walikota Sibolga, Walikota Tanjung Balai, Bupati Nias Utara, Walikota Binjai.</p>\r\n\r\n<p>Perwakilan Ombudsman RI Dadang S Suharmawijaya mengucapkan terima kasih kepada seluruh daerah yang ada di Provinsi Sumatra Utara dapat hadir di acara Penyerahan hasil Penilaian Penyelenggaraan Pelayanan Publik di Kantor Ombudsman RI Perwakilan Sumatera Utara.&nbsp;</p>\r\n\r\n<p>&quot;Tentunya tugas utama kami menerima pengaduan-pengaduan masyarakat demi perbaikan pelayanan publik di daerah-daerah demi memberikan penilaian-penilaian serta meningkatkan perkembangan-perkembangan di Kabupaten/Kota yang saudara pimpin,&quot; katanya.&nbsp;</p>\r\n\r\n<p>&quot;Hal ini untuk kemajuan yang sangat relatif bukan membanding-bandingkan namun meningkatkan kualitas demi kepatuhan dalam menjalani pelayanan publik kepada masyarakat,&quot; sambungnya.</p>\r\n\r\n<p>Sementara itu, Gubernur Sumatera Utara &nbsp;Edy Rahmayadi berharap kepada seluruh kabupaten/kota yang ada di Provinsi Sumatera Utara utuk sering melakukan sharing dan konsultasi dengan Ombudsman, bukan mencari kebenaran melainkan demi menjadikan dan mewujudkan pelayanan publik kepada masyarakat.</p>\r\n\r\n<p>&quot;Bukan persoalan Hijau, Kuning dan Merah namun kita harus positif, serius dengan segala keterbatasan serta jangan membelokan kekuasaan demi pelayanan publik di daerah kita,&quot; ujarnya.&nbsp;</p>\r\n\r\n<p>Gubernur Sumatera Utara juga mengajak kepada seluruh elemen masyarakat Sumatera Utara untuk turut serta mensukseskan acara Internasional di Danau Toba.&nbsp;</p>\r\n\r\n<p>&quot;Nantinya tetap tingkatkan budaya kita, yakni sopan santun, karena tamu tidak hanya nasional melainkan tamu mancanegara,&quot; ucapnya.</p>\r\n\r\n<p><span style=\"font-size:10px\"><em>Sumber :&nbsp;https://tebingtinggikota.go.id/berita/berita-daerah/pemko-tebing-tinggi-raih-zona-hijau-kepatuhan-standar-pelayanan-publik-tahun-2022-pj-wali-kota-terima-penghargaan-dari-ombudsman-ri-perwakilan-sumut</em></span></p>\r\n',NULL,'ombudsman,pelayanan publik,tebing tinggi,2022',0,40,NULL,0,NULL,'admin','2023-05-01 21:33:43','admin','2023-05-01 21:33:43'),
	(12,1,NULL,'2023-05-01','Permudah Izin Usaha, Kementan Sosialisasikan Perppu Nomor 2 Tahun 2022 ','permudah-izin-usaha-kementan-sosialisasikan-perppu-nomor-2-tahun-2022','<p>Kementerian Pertanian (Kementan) melalui Direktorat Jenderal Perkebunan (Ditjenbun) melakukan sosialisasi Peraturan Pemerintah Pengganti Undang-undang (Perppu) Nomor 2 Tahun 2022 tentang Cipta Kerja dan Perizinan Berusaha Subsektor Perkebunan.</p>\r\n\r\n<p>Sosialisasi itu bertujuan agar seluruh stakeholder memiliki pandangan dan pemahaman yang sama terhadap seluruh peraturan subsektor perkebunan yang terdampak dengan terbitnya Perppu.</p>\r\n\r\n<p>Sebagai informasi, Perppu Nomor 2 Tahun 2022 tentang Cipta Kerja mengubah beberapa norma dalam Undang-undang (UU) Nomor 39 Tahun 2014 tentang Perkebunan. Perubahan ini dilakukan karena memberikan dampak yang cukup signifikan terhadap peraturan-peraturan di lingkup subsektor perkebunan.</p>\r\n\r\n<p>Sekretariat Direktorat Jenderal (Ditjen) Perkebunan Heru Tri Widarto mengatakan, perppu tersebut bertujuan untuk memberikan kemudahan perizinan berusaha pada budi daya pertanian skala tertentu. Menurutnya, perubahan ini sebagai penyederhanaan dalam pertimbangan penetapan batasan luas lahan untuk usaha perkebunan, fasilitasi pembangunan kebun masyarakat sekitar, dan pelepasan varietas perbenihan perkebunan.</p>\r\n\r\n<p>&ldquo;Perppu ini adalah pengganti UU Nomor 11 Tahun 2020 tentang Cipta Kerja yang telah mengamanatkan mekanisme penetapan jenis perizinan berusaha di Indonesia dengan menggunakan pendekatan berbasis risiko sebagai solusi penyederhanaan proses perizinan dengan tetap menggunakan sistem online single submission (OSS),&rdquo; ungkap Heru melalui keterangan persnya, Kamis (9/3/2023).</p>\r\n\r\n<p>Heru menjelaskan, Kementan telah memangkas sejumlah perizinan berusaha, menerapkan konsep kemudahan berusaha, dan memberi perlakuan khusus kepada pelaku usaha mikro kecil menengah (UMKM). &ldquo;Namun, kemudahan perizinan berusaha ini akan diimbangi dengan penguatan pengawasan di lapangan, sehingga pelaku usaha dalam menjalankan usahanya tidak melakukan tindakan diluar aturan yang ada,&rdquo; jelas Heru.</p>\r\n\r\n<p><span style=\"font-size:10px\"><em>Sumber :&nbsp;https://money.kompas.com/read/2023/03/09/105140726/permudah-izin-usaha-kementan-sosialisasikan-perppu-nomor-2-tahun-2022</em></span></p>\r\n',NULL,'izin usaha,kementan,perppu',0,143,NULL,0,NULL,'admin','2023-05-01 21:40:59','admin','2023-05-01 21:40:59'),
	(16,5,NULL,'2023-05-01','Standar Pelayanan','standar-pelayanan','<object data=\"http://eperizinan.tebingtinggikota.go.id/v2/assets/media/uploads/SK_Standar_Pelayanan_2022.pdf#navpanes=0\" type=\"application/pdf\" width=\"100%\" height=\"800px\">\r\n      <p>Unable to display PDF file.</p>\r\n    </object>',NULL,NULL,0,52,NULL,0,NULL,'operator','2023-05-30 15:35:42','operator','2023-05-30 15:35:42'),
	(21,5,NULL,'2023-05-01','Struktur Organisasi','struktur-organisasi','<object data=\"http://eperizinan.tebingtinggikota.go.id/v2/assets/media/uploads/SK_Standar_Pelayanan_2022.pdf#navpanes=0\" type=\"application/pdf\" width=\"100%\" height=\"800px\">\r\n      <p>Unable to display PDF file.</p>\r\n    </object>',NULL,NULL,0,10,NULL,0,NULL,'admin','2023-06-15 09:36:51','admin','2023-06-15 09:36:51'),
	(23,5,NULL,'2023-05-01','Maklumat Pelayanan','maklumat-pelayanan','<div class=\"row\">\r\n<div class=\"col-lg-6 col-12\">\r\n<p>Maklumat pelayanan, adalah salah satu poin penilaian kepatuhan standar pelayanan publik sebagaimana UU No. 25 Tahun 2009 tentang Pelayanan Publik yang dilakukan Ombudsman RI. Jika merujuk pada Pasal 22 Ayat (1) UU Pelayanan Publik, mengamanahkan penyelenggara untuk menyusun dan menetapkan maklumat pelayanan, yang merupakan pernyataan kesanggupan penyelenggara dalam melaksanakan pelayanan sesuai dengan standar pelayanan&quot;. Lebih lanjut dalam ayat (2) ditegaskan bahwa Maklumat Pelayanan wajib dipublikasikan secara jelas dan luas.</p>\r\n\r\n<p>Dalam Peraturan Pemerintah No.96 Tahun 2012 tentang Pelaksanaan Undang-Undang Nomor 25 Tahun 2009 Tentang Pelayanan Publik, maklumat pelayanan diartikan sebagai pernyataan tertulis yang berisi keseluruhan rincian kewajiban dan janji yang terdapat dalam standar pelayanan. Maklumat pelayanan dapat diartikan sebagai bentuk kewajiban dan janji penyelenggara layanan, kepada masyarakat sebagai pengguna layanan, untuk melaksanakan standar pelayanan yang telah ditetapkan penyelenggara layanan.</p>\r\n</div>\r\n\r\n<div class=\"col-lg-6 col-12 mt-3 mt-lg-0\">\r\n<div class=\"job-thumb job-thumb-detail-box bg-white shadow-lg\" style=\"padding: 0 !important; max-width: 100%\"><img src=\"http://eperizinan.tebingtinggikota.go.id/v2/assets/media/image/maklumat-pelayanan.png\" style=\"width:100%\" /></div>\r\n</div>\r\n</div>\r\n',NULL,NULL,0,41,NULL,0,NULL,'admin','2023-06-15 09:54:01','admin','2023-06-15 10:15:27'),
	(24,2,NULL,'2023-08-10','Badan Kesbangpol Ajak Pemilih Muda Turut Sukseskan Pemilu 2024 melalui Pendidikan Politik','badan-kesbangpol-ajak-pemilih-muda-turut-sukseskan-pemilu-2024-melalui-pendidikan-politik','<p>Pemerintah Kota Tebing Tinggi melalui Badan Kesatuan Bangsa dan Politik menggelar acara Pendidikan Politik Pemilih Muda Kota Tebing Tinggi pada Kamis 10 Agustus 2023 di Cafe Legato, Jl. Imam Bonjol, Tebing Tinggi. Penjabat (Pj) Wali Kota Tebing Tinggi yang diwakilkan Pj. Sekretaris Daerah Kota Tebing Tinggi H. Kamlan Mursyid, S.H., M.M. membuka acara sekaligus mengajak para pemilih muda Kota Tebing Tinggi untuk turut serta dalam menyukseskan pesta demokrasi Pemilu dan Pilkada serentak 2024.</p>\r\n\r\n<p>&quot;Kita ketahui bersama bahwa pada tahun 2024 kita akan melaksanakan pesta demokrasi terbesar dan terumit sepanjang sejarah Indonesia, yaitu Pemilihan Umum (Pemilu) dan Pemilihan Kepala Daerah (Pilkada) serentak tahun 2024. Untuk itu mari adik-adik semua kita bersama-sama sukseskan pemilu ini,&quot; ujar Pj. Sekretaris Daerah.<br />\r\n<br />\r\nSebelumnya, Pj. Sekretaris Daerah mengungkapkan bahwa salah satu penyebab utama tergerusnya toleransi pada pemilu 2019 yang lalu adalah penyebaran informasi hoax di internet dan media sosial, dengan persentase sebesar 37,6 persen.</p>\r\n\r\n<p>Acara dirangkai dengan pembagian Bendera Merah Putih secara simbolis dalam menyambut hari kemerdekaan Republik Indonesia dan dilanjutkan dengan foto bersama. Turut hadir, Staf Ahli Bidang Ekonomi, Pembangunan dan Keuangan Muhammad Syah Irwan, SKM, M. Kes selaku narasumber, Praktisi Media / Ilmu Komunikasi Fisip USU Irsan Mulyadi, S.Sos. M.I.Kom, CO. Founder Tebing Tinggiku / Founder Putra-Putri Tebing Tinggi, Para Pimpinan Ormas pelajar / mahasiswa dan pemuda yang hadir, serta para undangan.</p>\r\n',NULL,'pemilu,pilkada,politik,pemuda',0,94,NULL,0,NULL,'admin','2023-09-27 13:01:03','admin','2023-09-27 14:18:32'),
	(25,2,NULL,'2023-08-12','Gerakan Pembagian Bendera Merah Putih Tahun 2023','gerakan-pembagian-bendera-merah-putih-tahun-2023','<p>Kegiatan Gerakan Pembagian 10 Juta Bendera Merah Putih di Kota Tebing Tinggi Tahun 2023 diselenggarakan dalam rangka menyemarakkan Peringatan HUT RI Ke-78 Tahun 2023 di Kota Tebing Tinggi. Kegiatan tersebut diselenggarakan pada tanggal 1 Agustus 2023 pada pukul 08.00 Wib s.d selesai dan berlokasi di Kantor Camat Rambutan Jl. Gunung Leuser BP7, Kota Tebing Tinggi.</p>\r\n\r\n<p>Pemerintah Kota Tebing Tinggi memberikan sejumlah 1000 buah bendera merah putih kepada masyarakat Kota Tebing Tinggi. Bendera merah putih diberikan kepada 20 orang perwakilan semua elemen, baik tokoh masyarakat, Kepala Sekolah, Kepala Lingkungan (Kepling), TP PKK Kota/Kelurahan. kalangan akademisi maupun masyarakat.</p>\r\n\r\n<p>Kegiatan Gerakan Pembagian 10 Juta Bendera Merah Putih di Kota Tebing Tinggi juga dihadiri dan disaksikan oleh Pimpinan Forkopimda Kota Tebing &nbsp;Tinggi, Pimpinan OPD Pemerintah Kota Tebing Tinggi, Camat dan Lurah, TP PKK Kota/Kelurahan, perwakilan perbankan, Tokoh Agama, Tokoh Masyarakat, insan pers dan tamu undangan serta masyarakat Kota Tebing Tinggi.</p>\r\n\r\n<p>@GAMBAR1@&nbsp; &nbsp;@GAMBAR2@</p>\r\n\r\n<p>Selain itu, Gerakan Pembagian 10 Juta Bendera Merah Putih di Kota Tebing Tinggi juga dilakukan dalam berbagai acara dan momen yang dilakukan oleh berbagai instansi pemerintah maupun swasta yang ada di Kota Tebing Tinggi. Yaitu diantaranya oleh Polres Kota Tebing Tinggi, Koramil 13 Tebing Tinggi, Badan Perencanaan Pembangunan Daerah (Bappeda) Kota Tebing Tinggi, Badan Kesatuan Bangsa dan Politik Kota Tebing Tinggi dalam rangkaian kegiatan Pendidikan Politik kepada Pelajar di Kota Tebing Tinggi, PT. Darmasindo, serta di berbagai kecamatan dan kelurahan di Kota Tebing Tinggi.<br />\r\n&nbsp;</p>\r\n',NULL,'HUT RI 78,Bendera,Merah Putih,Wawasan Kebangsaan',0,4,NULL,0,NULL,'admin','2023-09-27 14:24:12','admin','2023-09-27 14:33:35'),
	(26,2,NULL,'2023-08-15','Pengukuhan Paskibraka Kota Tebing Tinggi Tahun 2023','pengukuhan-paskibraka-kota-tebing-tinggi-tahun-2023','<p>Pada hari Selasa, 15 Agustus 2023 Penjabat (Pj.) Wali Kota Tebing Tinggi Drs. Syarmadani, M.Si. mengukuhkan sebanyak 45 orang Paskibraka (Pasukan Pengibar Bendera Pusaka)dan 37 orang Korps Musik Kota Tebing Tinggi yang akan bertugas saat upacara peringatan HUT Kemerdekaan ke-78 Republik Indonesia tahun 2023. Pengukuhan dilakukan di Gedung GOR Asber, Jl. Gunung Leuser Kota Tebing Tinggi.<br />\r\n<br />\r\nDalam rangkaian acara pengukuhan oleh Pj. Wali Kota selaku Pembina Upacara, didahului dengan pembacaan pengantar pengukuhan oleh Kepala Badan Kesbangpol Zubir Husni Harahap, pembacaan dan penandatanganan kata pengukuhan, penyematan tanda pangkat, penandatanganan kata-kata pengukuhan, pemasangan kendit Paskibraka dan selempang Korps oleh Pembina Upacara. Dilanjutkan serah terima tugas dan tanggungjawab anggota Paskibraka tahun 2022 kepada anggota Paskibraka 2023 dan berfoto bersama.</p>\r\n',NULL,'Paskibraka,Merah Putih,HUT RI 78',0,3,NULL,0,NULL,'admin','2023-09-27 14:37:06','admin','2023-09-27 14:37:06'),
	(27,2,NULL,'2023-08-16','Apel Kehormatan dan Renungan Suci Kota Tebing Tinggi','apel-kehormatan-dan-renungan-suci-kota-tebing-tinggi','<p>Rabu, 16 Agustus 2023 Pukul 23.59 WIB. Dalam rangka memperingati HUT Kemerdekaan RI Ke-78, Pemerintah Kota Tebing Tinggi melaksanakan giat Apel Kehormatan dan Renungan Suci yang dihadiri oleh unsur Forkopimda, TNI, POLRI, ASN, Pramuka dan Korps Musik bertempat di Taman Makam Bahagia Kota Tebing Tinggi.<br />\r\n<br />\r\nAcara ini digelar untuk mengenang dan menghormati jasa para pahlawan yang telah wafat memperjuangkan Kemerdekaan Indonesia.<br />\r\n&nbsp;</p>\r\n',NULL,'Apel,Renungan Suci,HUT RI 78,Kemerdekaan RI',0,3,NULL,0,NULL,'admin','2023-09-27 14:40:02','admin','2023-09-27 14:40:02'),
	(28,2,NULL,'2023-08-17','Upacara Detik-Detik Proklamasi Kemerdekaan RI Tahun 2023','upacara-detik-detik-proklamasi-kemerdekaan-ri-tahun-2023','<p>Pada hari Kamis, 17 Agustus 2023 Pukul 09.30 WIB telah dilaksanakan Upacara Peringatan Detik - Detik Proklamasi Kemerdekaan Republik Indonesia bertempat di Lapangan Merdeka Kota Tebing Tinggi.<br />\r\n<br />\r\nAcara dihadiri oleh seluruh jajaran Pemerintah Kota Tebing Tinggi, Forkopimda, TNI, POLRI, ASN, Ormas, Pelajar, dan tamu undangan lainnya.<br />\r\n<br />\r\nDirgahayu Republik Indonesia Ke-78, Terus Melaju Untuk Indonesia Maju! Merdeka!</p>\r\n',NULL,'Upacara,Proklamasi,Kemerdekaan RI',0,0,NULL,0,NULL,'admin','2023-09-27 14:46:40','admin','2023-09-27 14:46:40'),
	(29,2,NULL,'2023-09-01','Penandatanganan Pakta Integritas Netralitas ASN  Menjelang Pemilu 2024','penandatanganan-pakta-integritas-netralitas-asn-menjelang-pemilu-2024','<p>Pada hari Jumat, 01 September 2023 dilaksanakan sosialisasi dan penandanganan Pakta Integritas Netralitas ASN di Lingkungan Badan Kesatuan Bangsa dan Politik Kota Tebing Tinggi bertempat di Aula Badan Kesbangpol Kota Tebing Tinggi Jl. Gn. Leuser dan dihadiri oleh Kepala Badan Kesatuan Bangsa dan Politik, Pejabat Administrator, Pejabat Pengawas serta Pelaksana.<br />\r\n<br />\r\nKegiatan ini dilaksanakan dalam rangka menindaklanjuti Surat Keputusan Bersama (SKB) Menteri PAN &amp; RB, Menteri Dalam Negeri, Kepala BKN, Ketua KASN, dan Ketua BAWASLU No. 2 Tahun 2022, No. 800-5474 Tahun 2022, No. 246 Tahun 2022, No. 30 Tahun 2022 dan No. 1447.1/PM.01/K.1/09/2022 Tentang Pedoman Pembinaan dan Pengawasan Netralitas Pegawai ASN Dalam Penyelenggaraan Pemilihan Umum dan Pemilihan.<br />\r\n<br />\r\nDalam kegiatan ini disampaikan dengan tegas kepada ASN menjelang Pemilu 2024, agar menerapkan prinsip netralitas, menghindari konflik kepentingan, menggunakan sosial media dengan bijak, dan menolak politik uang.</p>\r\n',NULL,'ASN,Pemilu,Netralitas',0,0,NULL,0,NULL,'admin','2023-09-27 14:50:04','admin','2023-09-27 14:50:04'),
	(30,2,NULL,'2023-09-15','Studi Wisata Paskibraka Kota Tebing Tinggi Tahun 2023','studi-wisata-paskibraka-kota-tebing-tinggi-tahun-2023','<p>Pemerintah Kota Tebing Tinggi melalui Badan Kesatuan Bangsa dan Politik melaksanakan kegiatan Studi Wisata bersama Paskibraka Kota Tebing Tinggi beserta Pelatih dan Tim Regu Kawal Upacara HUT Kemerdekaan RI Ke - 78 Tahun 2023 bertempat di&nbsp;Danau Toba, Parapat, pada 13 - 14 September 2023<br />\r\n<br />\r\nTurut hadir Pj. Wali Kota Tebing Tinggi yang diwakili oleh Asisten I Bapak Drs. Bambang Sudaryono dan Kadis Pemuda, Olahraga dan Pariwisata Bapak Syahdama Yanto, AP.<br />\r\n<br />\r\nDalam kata sambutan yang disampaikan di Mess Pora-Pora Pemprov. Sumatera Utara, Bapak Bambang Sudaryono berpesan kepada Paskibraka Kota Tebing Tinggi Tahun 2023 agar memanfaatkan momen ini untuk menikmati keindahan alam Danau Toba sekaligus menambah semangat dalam menjalankan sisa tugas sebagai Paskibraka Kota Tebing Tinggi.</p>\r\n',NULL,'Paskibraka,Wisata,Tebing Tinggi',0,3,NULL,0,NULL,'admin','2023-09-27 15:03:00','admin','2023-09-27 15:03:00');

/*!40000 ALTER TABLE `_posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL,
  `RoleName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Operator'),
	(3,'Publik');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `SettingID` int(10) unsigned NOT NULL,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`Uniq`, `SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,1,'SETTING_WEB_NAME','SETTING_WEB_NAME','KESBANGPOL'),
	(2,2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Website Resmi\r\nBadan Kesatuan Bangsa dan Politik\nPemerintah Kota Tebing Tinggi'),
	(3,3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,4,'SETTING_ORG_NAME','SETTING_ORG_NAME','Badan Kesatuan Bangsa dan Politik'),
	(5,5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','Jl. Gunung Agung, Kec. Rambutan, Kota Tebing Tinggi, Sumatera Utara 20615'),
	(6,6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','085359867032'),
	(9,9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','kesbangpol.tebingtinggi@gmail.com'),
	(11,11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(14,14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','main.gif'),
	(15,15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.01'),
	(16,16,'SETTING_ORG_REGION','SETTING_ORG_REGION','PEMERINTAH KOTA TEBING TINGGI'),
	(17,17,'SETTING_API_SICANTIK','SETTING_API_SICANTIK','https://sicantik.go.id/api/TemplateData/keluaran/40532.json'),
	(18,18,'SETTING_LINK_INSTAGRAM','SETTING_LINK_INSTAGRAM','https://www.instagram.com/kesbangpol.tebingtinggi/'),
	(19,19,'SETTING_LINK_FACEBOOK','SETTING_LINK_FACEBOOK',''),
	(20,20,'SETTING_LINK_YOUTUBE','SETTING_LINK_YOUTUBE',''),
	(21,21,'SETTING_LINK_GOOGLEMAP','SETTING_LINK_GOOGLEMAP','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3982.9888016791406!2d99.16179397463573!3d3.35288059662184!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3031604110206cd9%3A0xc9f75c7f136009c8!2sBadan%20Kesatuan%20Bangsa%2C%20Politik%20%26%20Perlindungan%20Masyarakat!5e0!3m2!1sen!2sid!4v1695784751645!5m2!1sen!2sid\" width=\"100%\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>'),
	(22,22,'SETTING_LINK_WHATSAPP','SETTING_LINK_WHATSAPP','6285359867032'),
	(23,23,'SETTING_LINK_INSTAGRAM_ACC','SETTING_LINK_INSTAGRAM_ACC','@kesbangpol.tebingtinggi');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNo` varchar(50) DEFAULT NULL,
  `ImgFile` varchar(250) DEFAULT NULL,
  `RegDate` date DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`Uniq`, `UserName`, `Email`, `Name`, `IdentityNo`, `BirthDate`, `Gender`, `Address`, `PhoneNo`, `ImgFile`, `RegDate`)
VALUES
	(1,'admin','administrator@eperizinan.tebingtinggikota.go.id','Administrator',NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17');

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`Uniq`, `UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	(1,'admin','3798e989b41b858040b8b69aa6f2ce90',1,0,'2023-05-09 08:15:14','::1');

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mcert
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mcert`;

CREATE TABLE `mcert` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IzinNama` varchar(50) DEFAULT NULL,
  `IzinRemarks1` text COMMENT 'Persyaratan',
  `IzinRemarks2` text COMMENT 'Lain-lain',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mcert` WRITE;
/*!40000 ALTER TABLE `mcert` DISABLE KEYS */;

INSERT INTO `mcert` (`Uniq`, `IzinNama`, `IzinRemarks1`, `IzinRemarks2`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'Izin Fisioterapy','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(2,'Izin Kerja Analisis','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(3,'Izin Kerja Bidan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(4,'Izin Kerja Bidan Sementara','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(5,'Izin Kerja Kesehatan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(6,'Izin Kerja Kesehatan Sementara','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(7,'Izin Kerja Perawat','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(8,'Izin Kerja Perawat Sementara','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(9,'Izin Kerja Tenaga Kefarmasian','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(10,'Izin Mendirikan Bangunan Tower','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(11,'Izin Operasional Rumah Sakit Kelas C dan D','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(12,'Izin Pemakaian Kekayaan Alat Berat','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(13,'Izin Pemasangan Jaringan Instalasi diatas / dibawa','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(14,'Izin Pendirian Program atau Satuan Pendidikan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(15,'Izin Pendirian Satuan Pendidikan Anak Usia Dini (P','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(16,'Izin Penyelenggaraan Optikal','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(17,'Izin Praktek Apoteker','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(18,'Izin Praktek Apoteker test','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(19,'Izin Praktek Dokter Gigi','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(20,'Izin Praktek Dokter Hewan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(21,'Izin Praktek Dokter Internsip','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(22,'Izin Praktek Dokter Spesialis Sementara','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(23,'Izin Praktek Dokter Spesialis test','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(24,'Izin Praktek Dokter Umum Sementara','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(25,'Izin Praktek Tenaga Kesehatan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(26,'Izin Praktik / Kerja Fisioterapi','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(27,'Izin Praktik Ahli Teknologi Laboratorium Medik','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(28,'Izin Praktik Bidan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(29,'Izin Reklame','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(30,'Izin Trayek Angkutan Kota','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(31,'Izin Tukang Gigi','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(32,'Izin Unit Transfusi Darah','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(33,'Komitmen Izin Koperasi Simpan Pinjam','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(34,'Komitmen Izin Lembaga Pelatihan Kerja','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(35,'Komitmen Izin Lingkungan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(36,'Komitmen Izin Operasional Klinik','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(37,'Komitmen Izin Operasional Pengelolaan Limbah Bahan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(38,'Komitmen Izin Operasional Rumah Sakit','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(39,'Komitmen Izin Pembuangan Air Limbah','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(40,'Komitmen Izin Pendirian Program atau Satuan Pendid','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(41,'Komitmen Izin Penyelenggaraan Angkutan Orang','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(42,'Komitmen Izin Penyelenggaraan Satuan Pendidikan No','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(43,'Komitmen Izin Usaha Industri','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(44,'Komitmen Izin Usaha Jasa Konstruksi','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(45,'Komitmen Izin Usaha Peternakan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(46,'Komitmen Sertifikat Higiene Sanitasi Pangan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(47,'Komitmen Sertifikat Pangan Produksi Rumah Tangga','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(48,'Komitmen Sertifikat Produksi Perusahaan Rumah Tang','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(49,'Komitmen Surat Izin Usaha Perdagangan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(50,'Komitmen Surat Keterangan Perdagangan Minuman Bera','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(51,'Komitmen Surat Pernyataan Kesanggupan Pengelolaan ','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(52,'Komitmen Tanda Daftar Gudang','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(53,'Komitmen Tanda Daftar Usaha Pariwisata','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(54,'Pencabutan Surat Izin Praktek Apoteker','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(55,'Perubahan Email / Pencabutan NIB OSS','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(56,'Prototype Perizinan Terintegrasi','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(57,'Sertifikat Laik Higiene Sanitasi Depot Air Minum','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(58,'Sertifikat Standar Izin Apotek','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(59,'Sertifikat Standar Izin Toko Obat','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(60,'Sertifikat Standar Klinik','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(61,'Surat Keterangan Pemeriksaan Kualitas Air Laborato','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(62,'Surat Terdaftar Penyehat Tradisional','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL);

/*!40000 ALTER TABLE `mcert` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mpegawai
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mpegawai`;

CREATE TABLE `mpegawai` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `PegNama` varchar(50) DEFAULT NULL,
  `PegNIP` varchar(50) DEFAULT NULL,
  `PegJabatan` varchar(50) DEFAULT NULL,
  `PegIsAktif` tinyint(1) DEFAULT '1',
  `PegImgPath` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tformulir
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tformulir`;

CREATE TABLE `tformulir` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `FormNama` varchar(200) DEFAULT '',
  `FormKeterangan` varchar(200) NOT NULL DEFAULT '',
  `FormAttachment` varchar(200) DEFAULT '',
  `CreatedOn` datetime DEFAULT NULL,
  `CreatedBy` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tlapor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tlapor`;

CREATE TABLE `tlapor` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `LaporNama` varchar(200) NOT NULL DEFAULT '',
  `LaporNIK` varchar(200) DEFAULT NULL,
  `LaporHP` varchar(50) NOT NULL DEFAULT '',
  `LaporEmail` varchar(50) NOT NULL DEFAULT '',
  `LaporKategori` varchar(200) NOT NULL DEFAULT '',
  `LaporIsi` text NOT NULL,
  `LaporStatus` enum('DITERIMA','PROSES','SELESAI') NOT NULL DEFAULT 'DITERIMA',
  `LaporKeterangan` text,
  `CreatedOn` datetime NOT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `UpdateBy` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tlapor` WRITE;
/*!40000 ALTER TABLE `tlapor` DISABLE KEYS */;

INSERT INTO `tlapor` (`Uniq`, `LaporNama`, `LaporNIK`, `LaporHP`, `LaporEmail`, `LaporKategori`, `LaporIsi`, `LaporStatus`, `LaporKeterangan`, `CreatedOn`, `UpdatedOn`, `UpdateBy`)
VALUES
	(1,'Yoel Rolas Simanjuntak','1271031208950002','085359867032','yoelrolas@gmail.com','Lainnya','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','DITERIMA',NULL,'2023-05-08 23:33:24',NULL,NULL),
	(2,'Rolas','1271031208950002','085359867032','yoelrolas@gmail.com','Lainnya','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','PROSES','Laporan ini sedang diproses. Kami akan informasikan hasilnya segera. Terimakasih.','2023-05-08 23:33:40','2023-05-09 00:41:08','admin'),
	(3,'Partopi Tao','1271031208950002','085359867032','yoelrolas@gmail.com','Lainnya','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','SELESAI','Laporan anda telah ditindaklanjut oleh bidang terkait. Terimakasih atas partisipasi anda.','2023-05-08 23:34:25','2023-05-09 00:39:24','admin');

/*!40000 ALTER TABLE `tlapor` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trequest
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trequest`;

CREATE TABLE `trequest` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL DEFAULT '',
  `IzinID` bigint(10) unsigned NOT NULL,
  `ReqRemarks` text,
  `ReqStatus` enum('DITERIMA','PROSES','SELESAI') NOT NULL DEFAULT 'DITERIMA',
  `ReqFile1` varchar(200) DEFAULT NULL,
  `ReqFile2` varchar(200) DEFAULT NULL,
  `ReqFile3` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table trequest_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trequest_log`;

CREATE TABLE `trequest_log` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ReqID` bigint(10) unsigned NOT NULL,
  `ReqStatus` varchar(50) NOT NULL DEFAULT '',
  `ReqRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
